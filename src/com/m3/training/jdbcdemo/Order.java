package com.m3.training.jdbcdemo;

public class Order {
	
	private String rootOrdid;
	private String parentOrdid;
	private int ordqty;
	
	public String getRootOrdid() {
		return rootOrdid;
	}
	public void setRootOrdid(String rootOrdid) {
		this.rootOrdid = rootOrdid;
	}
	public String getParentOrdid() {
		return parentOrdid;
	}
	public void setParentOrdid(String parentOrdid) {
		this.parentOrdid = parentOrdid;
	}
	public int getOrdqty() {
		return ordqty;
	}
	public void setOrdqty(int ordqty) {
		this.ordqty = ordqty;
	}
	
	public String toString() {
		StringBuilder answer = new StringBuilder();
		answer.append("Order: \n");
		answer.append("Order quantity: ").append(getOrdqty()).append("\n");
		answer.append("Order parent: ").append(getParentOrdid()).append("\n");
		answer.append("Order id: ").append(getRootOrdid()).append("\n");

		answer.append("\n");
		return answer.toString();
	}

}
