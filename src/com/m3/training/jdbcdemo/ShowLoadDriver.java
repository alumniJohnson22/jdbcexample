package com.m3.training.jdbcdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ShowLoadDriver {
	
	public static void main(String[] args) {
	
		List<Order> orders = new ArrayList<>();
		// first, try to load the class
		try {
			// put this in a properties file
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("Class loaded");
		} catch (ClassNotFoundException e) {
			System.out.println("Could not find class");
			// should we consume this error or peace out?
			return;
		}

		// Next, try to get a connection from the static 
		// class DriverManager
		String connectionString = 
				"jdbc:oracle:thin:@10.20.40.53:1521:oradb1";
		connectionString = 
				"jdbc:oracle:thin:@88.211.122.42:1521:oradb1";
		String uid = "delegate";
		String pwd = "pass";
		// should these things be hard coded? 
		// NOOOOO you must use a properties file
		// Driver Manager is a concrete class
		try (Connection connection = 
				DriverManager.getConnection(connectionString, uid, pwd)) {
			// Create a statement
			Statement stmt = connection.createStatement();
			// put this in a properties file, this does not belong in java
			// This text is SQL, the writer of this code should be familiar with SQL
			// we will solve this in a different way later
			String query = "select rootordid, parentordid, orderqty from posttrade.orders";
			if (stmt.execute(query)) {
				ResultSet rs = stmt.getResultSet();
				while (rs.next()) {
					String parentOrdid = rs.getNString("parentordid");
					String rootOrdid = rs.getNString("rootordid");
					int qty = rs.getInt("orderqty");
					System.out.println(parentOrdid + " " + rootOrdid + " " + qty);
					Order order = new Order();
					order.setParentOrdid(parentOrdid);
					order.setRootOrdid(rootOrdid);
					order.setOrdqty(qty);
					orders.add(order);
				}
			}
			System.out.println("ok query");
		} catch (SQLException e) {
			System.out.println(e.getMessage() + " Could not connect, or other sql problem, same comment as above");
			return;
		}
		
		// modern foreach syntax
		orders.forEach(order -> { System.out.println(order); });
		
		
		
	}

}
