package com.m3.training.jdbcdemo;

public class OrderDAO {
	// choice 1
	// establish a connection, use it, then dispose of it
	
	// choice 2
	// constructor creates a database connection?
	// must close connection at end of day
	// every time a request comes in, check that the connection is still live
	
	//choice 3
	// connection pool is best, but we can leave that code aside if you wish
	
	// CRUD, create an order on the database, read an order from the database, 
	// update an order that exists, delete a record that represents an order
	
	// write a method that returns 
		//a datastructure of orders, each one inflated from the result set
	
	// write another method that accepts an order object
		// and updates the database
	// use a properties file
	

}
